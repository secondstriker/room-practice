package com.codewithmohsen.roompractice

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent


@Module
@InstallIn(ViewModelComponent::class)
interface NoteRepositoryModule {

    @NoteRepositoryAnnotation1
    @Binds
    fun bindsNoteRepository1(impl: NoteRepositoryImpl1): NoteRepository

    @NoteRepositoryAnnotation2
    @Binds
    fun bindsNoteRepository2(impl: NoteRepositoryImpl2): NoteRepository
}