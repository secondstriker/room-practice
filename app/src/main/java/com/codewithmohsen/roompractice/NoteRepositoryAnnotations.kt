package com.codewithmohsen.roompractice

import javax.inject.Qualifier

@Retention(AnnotationRetention.RUNTIME)
@Qualifier
annotation class NoteRepositoryAnnotation1

@Retention(AnnotationRetention.RUNTIME)
@Qualifier
annotation class NoteRepositoryAnnotation2