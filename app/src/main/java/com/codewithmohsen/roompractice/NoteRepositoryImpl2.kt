package com.codewithmohsen.roompractice

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class NoteRepositoryImpl2 @Inject constructor(): NoteRepository {

    override fun getNotes(): Flow<List<Note>> {
        return flow {
            emit(listOf(Note("title 1"), Note("title 2")))
        }
    }

    override fun insert(note: Note) {
        println("note $note inserted")
    }
}