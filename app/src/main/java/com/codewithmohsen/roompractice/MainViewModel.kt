package com.codewithmohsen.roompractice

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    @NoteRepositoryAnnotation1 private val repository: NoteRepository) : ViewModel() {

//    init {
//        insertJunk()
//    }
//
//    private fun insertJunk() {
//        viewModelScope.launch(Dispatchers.IO) {
//            repository.insert(Note("task 1"))
//            repository.insert(Note("task 2"))
//            repository.insert(Note("task 3"))
//            repository.insert(Note("task 4"))
//        }
//    }

    fun getNotes(): Flow<List<Note>> = repository.getNotes()

    fun insert(note: Note) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.insert(note)
        }
    }
}