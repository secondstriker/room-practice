package com.codewithmohsen.roompractice

import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class NoteRepositoryImpl1 @Inject constructor(private val dao: NoteDao): NoteRepository {
    override fun getNotes(): Flow<List<Note>> = dao.getNotes()
    override fun insert(note: Note) = dao.insert(note)
}