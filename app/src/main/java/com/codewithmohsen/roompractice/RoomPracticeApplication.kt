package com.codewithmohsen.roompractice

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class RoomPracticeApplication: Application() {
}