package com.codewithmohsen.roompractice

import kotlinx.coroutines.flow.Flow


interface NoteRepository {

    fun getNotes(): Flow<List<Note>>

    fun insert(note: Note)
}


